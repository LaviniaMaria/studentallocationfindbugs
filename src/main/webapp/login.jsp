<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f" %>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h" %>
<f:view>
<html>
<head>
    <title></title>
</head>
<body>
<h:form>
    <h:panelGrid columns="3" border="0"
                 cellpadding="5" cellspacing="3"
                 headerClass="login-heading" style="margin:0 auto">

        <h:outputLabel for="userNameInput">
            <h:outputText value="Username:"/>
        </h:outputLabel>
        <h:inputText id="userNameInput" size="20" maxlength="30"
                     required="true"
                     value="#{userBean.username}">
            <f:validateLength minimum="5" maximum="30"/>
        </h:inputText>
        <h:message for="userNameInput" styleClass="errors"/>

        <h:outputLabel for="passwordInput">
            <h:outputText value="Password:"/>
        </h:outputLabel>
        <h:inputSecret id="passwordInput" size="20" maxlength="20"
                       required="true"
                       value="#{userBean.password}">
            <f:validateLength minimum="5" maximum="15"/>
        </h:inputSecret>
        <h:message for="passwordInput" styleClass="errors"/>
        <h:panelGroup>
        <h:commandButton action="#{userBean.login}"
                         value="Login"/>

        <h:commandLink value="New user?" action="/register.jsp" immediate="true" />
        </h:panelGroup>
    </h:panelGrid>
</h:form>
</body>
</html>
</f:view>