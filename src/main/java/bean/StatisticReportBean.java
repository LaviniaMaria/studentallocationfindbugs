package bean;

import bridge.inteface.IProjectBridge;
import bridge.inteface.IStudentBridge;
import bridge.inteface.ITeacherBridge;
import dao.Factory;
import dto.Statistics;
import utils.Constants;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.HashMap;

@ManagedBean(name = "statisticReportBean")
@SessionScoped
public class StatisticReportBean extends AbstractReportBean {

    private Factory factory;
    private IStudentBridge studentBridge;
    private IProjectBridge projectBridge;
    private ITeacherBridge teacherBridge;

    public StatisticReportBean() {
        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        studentBridge = factory.getStudentDAO();
        projectBridge = factory.getProjectDAO();
        teacherBridge = factory.getTeacherDAO();

        reportFile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/report2");;
    }

    @Override
    public HashMap<String, Object> getReportParameters() {

        HashMap<String, Object> params = new HashMap<String, Object>();
        String studentsNo = studentBridge.getStudentsNo();
        String projectsNo = projectBridge.getProjectsNo();
        String teachersNo = teacherBridge.getTeachersNo();

        params.put(Constants.ReportParams.STUDENTS_NO, studentsNo);
        params.put(Constants.ReportParams.TEACHERS_NO, teachersNo);
        params.put(Constants.ReportParams.PROJECTS_NO, projectsNo);
        params.put(Constants.ReportParams.NOT_ACCEPTED_STUD,"4");

        return params;

    }

}
