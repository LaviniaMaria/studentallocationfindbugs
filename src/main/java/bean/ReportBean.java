/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import bridge.inteface.IProjectBridge;
import dao.Factory;
import dto.ProjectAssignment;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import java.util.List;

@ManagedBean(name = "reportBean")
@SessionScoped
public class ReportBean extends AbstractReportBean {

    Factory factory;
    IProjectBridge projectBridge;
    private List<ProjectAssignment> listOfProjects;

    public ReportBean() {
        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        projectBridge = factory.getProjectDAO();
        reportFile = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/reports/report1");
    }

    public List<ProjectAssignment> getListOfProjects() {
        listOfProjects = projectBridge.getProjectsAssign();
        return listOfProjects;
    }

    @Override
    protected JRDataSource getDataSource() 
    {
        return new JRBeanCollectionDataSource(getListOfProjects());
    }
    
}
