package bean;

import bridge.inteface.IProjectBridge;
import bridge.inteface.IStudPrefBridge;
import dao.Factory;
import dto.Project;
import org.primefaces.component.datatable.DataTable;
import utils.Constants;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

public class TableBean implements Serializable {

    private List<Project> projects;
    private DataTable dataTable;
    private IProjectBridge projectBridge;
    IStudPrefBridge studentPredBridge;
    private Factory factory;


    public TableBean() {
        System.out.println(": > > > " + UserBean.getTypeOfAccess());
        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        projectBridge = factory.getProjectDAO();
        studentPredBridge = factory.getStudPrefDAO();
        addProjects();
    }

    public List<Project> getProjects() {

        return  projects;
    }

    public void setProjects(List<Project> projects) {

        this.projects = projects;
    }

    public void addProjects() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        UserBean userBean = (UserBean) facesContext.getApplication()
                .evaluateExpressionGet(facesContext, "#{userBean}",
                        UserBean.class);
        projects = projectBridge.getProjects(userBean.getId());
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public String apply() {

        Project project = (Project) dataTable.getRowData();
        FacesContext facesContext = FacesContext.getCurrentInstance();

        UserBean userBean = (UserBean) facesContext.getApplication()
                .evaluateExpressionGet(facesContext, "#{userBean}",
                        UserBean.class);

        studentPredBridge.assignStudToProject(userBean.getId(), String.valueOf(project.getId()));

        return Constants.Navigation.APPLIED;

    }

}
