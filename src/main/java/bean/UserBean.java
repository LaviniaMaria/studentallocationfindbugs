/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import bridge.jdbc.UserBridge;
import dao.Factory;
import utils.Constants;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.io.Serializable;

public class UserBean implements Serializable {
    public static int TYPE_OF_ACCESS;

    private String username;
    private String password;
    private String type;
    private String id;
    private String db;

    public String getDb() {
        return db;
    }

    public void setDb(String db) {
        this.db = db;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
  
    public String login() {
        this.id = UserBridge.isUser(username, password, type);
        if (id != null) {
            FacesContext context = FacesContext.getCurrentInstance();
            HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
            session.setAttribute("userId", id);
            session.setAttribute("username", username);
            switch(db) {
                case "jdbc" :
                    TYPE_OF_ACCESS = Factory.JDBC; break;
                case "jpa":
                    TYPE_OF_ACCESS = Factory.JPA; break;

            }
            System.out.println("Type: " + db);

            switch (type) {
                case Constants.EntityType.STUDENT: {
                    return Constants.Navigation.SUCCESS_STUD;
                }
                case Constants.EntityType.TEACHER: {
                    return Constants.Navigation.SUCCESS_TEACHER;
                }
            }
            System.out.println("Before: " + db);

        }
        return Constants.Navigation.FAILURE;
    }

    public String logout() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession session
                = (HttpSession) facesContext.getExternalContext().
                getSession(false);

        if (session != null) {
            session.invalidate();
        }

        return Constants.Navigation.LOGOUT;
    }


    public static int getTypeOfAccess(){
        System.out.println("...: " + TYPE_OF_ACCESS);
        return TYPE_OF_ACCESS;
    }

}
