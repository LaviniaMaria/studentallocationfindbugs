/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import bridge.inteface.IProjectBridge;
import bridge.inteface.ITeacherPrefBridge;
import dao.Factory;
import dto.Project;
import dto.TeacherDTO;

import javax.faces.context.FacesContext;

/**
 * @author lavinia
 */
public class AddBean {

    Project project;
    Factory factory;
    IProjectBridge projectBridge;
    ITeacherPrefBridge teacherPrefBridge;

    public AddBean() {
        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        projectBridge = factory.getProjectDAO();
        teacherPrefBridge = factory.getTeacherPrefDAO();
        project = new Project();
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public String addProject() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        String userId = (String)facesContext.getExternalContext().getSessionMap().get("userId");
        System.out.println(userId);
        
        Long teacherId = Long.valueOf(userId);
        TeacherDTO teacher = teacherPrefBridge.getTeacherById(teacherId);
        project.setTeacher(teacher);
        projectBridge.addProject(project);
        return "added";
    }


}
