
package bean;

import bridge.inteface.IProjectBridge;
import dao.Factory;
import dto.Project;
import org.primefaces.component.datatable.DataTable;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.List;

public class TeacherViewBean implements Serializable{
    
    private List<Project> projects;
    private DataTable dataTable;
    private Project currentProject;
    IProjectBridge projectBridge;
    Factory factory;

    public TeacherViewBean() {

        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        projectBridge = factory.getProjectDAO();
        addProjects();
    }

    public List<Project> getProjects() {
        addProjects();
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }

    public void addProjects() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        UserBean userBean = (UserBean) facesContext.getApplication()
                .evaluateExpressionGet(facesContext, "#{userBean}",
                        UserBean.class);
        projects = projectBridge.getProjectsForTeacher(userBean.getId());
    } 

    public Project getCurrentProject() {
        return currentProject;
    }

    public void setCurrentProject(Project currentProject) {
        this.currentProject = currentProject;
    }
    
    public String view() {
        
        currentProject=(Project)dataTable.getRowData();
        return "view";
    }

    public String full() {

        currentProject=(Project)dataTable.getRowData();
        return "full";
    }
    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    
}
