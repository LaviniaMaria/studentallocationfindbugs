package bean;

import bridge.jpa.StudentDAO;
import dto.Student;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Alice on 12/14/2014.
 */
public class MigrationBean {
    EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("$objectdb/db/migration.odb");
    EntityManager em = emf.createEntityManager();



    public void copyAllStudents(){
        List<Student> students = new StudentDAO().getAllStudent();
        for(Student student: students){
            em.persist(student);
        }
    }
}
