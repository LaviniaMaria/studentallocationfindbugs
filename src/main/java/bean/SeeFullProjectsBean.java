package bean;

import bridge.inteface.IProjectBridge;
import bridge.inteface.ITeacherPrefBridge;
import bridge.jpa.StudPrefBridgeJPA;
import dao.Factory;
import dto.Project;
import org.primefaces.component.datatable.DataTable;

import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by Alice.
 */
public class SeeFullProjectsBean {
    private List<Project> projects;
    Factory factory;
    IProjectBridge projectBridge;
    ITeacherPrefBridge teacherPrefBridge;
    private DataTable dataTable;

    public SeeFullProjectsBean() {
        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        projectBridge = factory.getProjectDAO();
        teacherPrefBridge = factory.getTeacherPrefDAO();
        addProjects();
    }

    public List<Project> getProjects() {

        return  projects;
    }

    public void setProjects(List<Project> projects) {

        this.projects = projects;
    }


    public void addProjects() {
        FacesContext facesContext = FacesContext.getCurrentInstance();

        String userId = (String)facesContext.getExternalContext().getSessionMap().get("userId");
        System.out.println(userId);
        Long teacherId = Long.valueOf(userId);
        projects = new StudPrefBridgeJPA().getProjectsForTeacherThatAreFull(teacherId);
        new MigrationBean().copyAllStudents();
    }
    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }



}
