/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;

import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractReportBean {
    
    protected JasperPrint jasperPrint;
    protected String reportFile;
   
    public void createReport() throws JRException, IOException {

        Map<String, Object> params = getReportParameters();
        JRDataSource dataSource = getDataSource();
 
        jasperPrint=JasperFillManager.fillReport(reportFile + ".jasper",
                params, dataSource);

    }

    public void convertToPdf() throws JRException, IOException {
       /* JasperExportManager.exportReportToPdfFile(printReportFile,
                reportFile + ".pdf");*/

        HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        httpServletResponse.addHeader("Content-disposition", "attachment; filename=report.pdf");
        ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, servletOutputStream);
        FacesContext.getCurrentInstance().responseComplete();

    }

    public void displayReport() throws JRException, IOException {
        createReport();
        convertToPdf();
    }

    protected Map<String, Object> getReportParameters() {
        return new HashMap<String, Object>();
    }

    protected JRDataSource getDataSource() {
        return new JREmptyDataSource();
    }

}
