/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bean;

import bridge.inteface.IStudPrefBridge;
import bridge.inteface.ITeacherPrefBridge;
import bridge.jpa.StudPrefBridgeJPA;
import dao.Factory;
import dto.Project;
import dto.Student;
import org.primefaces.component.datatable.DataTable;

import javax.faces.context.FacesContext;
import java.util.List;

/**
 *
 * @author lavinia
 */
public class ViewBean {
    
    private List<Student> students;
    private DataTable dataTable;
    Factory factory;
    IStudPrefBridge studPrefBridge;
    ITeacherPrefBridge teacherPrefBridge;
    
    public ViewBean() {

        factory = Factory.getDAOFactory(UserBean.getTypeOfAccess());
        studPrefBridge = factory.getStudPrefDAO();
        teacherPrefBridge = factory.getTeacherPrefDAO();
        initList();
    }
    
    public void initList() {
        
        FacesContext facesContext = FacesContext.getCurrentInstance();
        TeacherViewBean teacherViewBean = (TeacherViewBean) facesContext.getApplication()
                .evaluateExpressionGet(facesContext, "#{teacherViewBean}",
                        TeacherViewBean.class);
        String projectId=String.valueOf(teacherViewBean.getCurrentProject().getId());
        students= studPrefBridge.getStudentsAt(projectId);

        //work in progress
        List<Project> projectFull = new StudPrefBridgeJPA().getProjectsForTeacherThatAreFull(1L);
        System.out.println("-----------");
        for(Project p: projectFull){
            System.out.println(p.getName() + " .. " + p.getCapacity());
        }
        
    }
    
    public void acceptStudent() {
        
        Student student=(Student)dataTable.getRowData();
       
        FacesContext facesContext = FacesContext.getCurrentInstance();
        TeacherViewBean teacherViewBean = (TeacherViewBean) facesContext.getApplication()
                .evaluateExpressionGet(facesContext, "#{teacherViewBean}",
                        TeacherViewBean.class);
        Project project=teacherViewBean.getCurrentProject();
        teacherPrefBridge.assignStudToProject(student,project);
        project.setStudentsAccepted(project.getStudentsAccepted()+1);
        
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }
    
    
}
