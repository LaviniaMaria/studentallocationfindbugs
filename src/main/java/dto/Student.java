/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lavinia
 */

@Entity
@Table(name = "student")
@NamedQueries(
        @NamedQuery(name=Student.FIND_ALL, query="from Student")
)
public class Student implements Serializable{
    public static final String FIND_ALL = "Student.findAll";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private String username;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {

        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column
    private String password;


    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name="stud_pref", joinColumns = @JoinColumn( name="stud_id"), inverseJoinColumns = @JoinColumn (name = "proj_id"))
    private List<Project> projectList = new ArrayList<Project>();

}
