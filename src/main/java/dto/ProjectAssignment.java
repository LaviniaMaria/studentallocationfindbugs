/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author lavinia
 */
import javax.persistence.*;


@SqlResultSetMappings( {
@SqlResultSetMapping(name = "ProjectAssignment", columns={
        @ColumnResult(name="studentName"), @ColumnResult(name="projectName"), @ColumnResult(name="teacherName")})
})
@javax.persistence.Entity
public class ProjectAssignment {


    private String studentName;
    private String projectName;
    private String teacherName;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }


    private String id;

    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
