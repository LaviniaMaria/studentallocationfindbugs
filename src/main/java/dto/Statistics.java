/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

/**
 *
 * @author lavinia
 */
public class Statistics {
    
    private String projectsNo;
    private String teachersNo;
    private String studentsNo;
    private String notAcceptedStud;

    public String getProjectsNo() {
        return projectsNo;
    }

    public void setProjectsNo(String projectsNo) {
        this.projectsNo = projectsNo;
    }

    public String getTeachersNo() {
        return teachersNo;
    }

    public void setTeachersNo(String teachersNo) {
        this.teachersNo = teachersNo;
    }

    public String getStudentsNo() {
        return studentsNo;
    }

    public void setStudentsNo(String studentsNo) {
        this.studentsNo = studentsNo;
    }

    public String getNotAcceptedStud() {
        return notAcceptedStud;
    }

    public void setNotAcceptedStud(String notAcceptedStud) {
        this.notAcceptedStud = notAcceptedStud;
    }
    
    
    
}
