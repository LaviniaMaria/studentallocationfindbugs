/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dto;

import javax.persistence.*;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lavinia
 */
@Entity
@Table( name = "project")
@NamedQueries({
        @NamedQuery(name = Project.FIND_ALL, query = "from Project"),
        @NamedQuery(name = Project.FIND_PROECTS_FOR_TEACHER, query = "from Project p where p.teacher.id=:teacher_id")
}
)
public class Project implements Serializable {

    public static final String FIND_ALL = "Project.findAll";
    public static final String FIND_PROECTS_FOR_TEACHER = "Project.findProjectsForTeacher";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private int capacity;

    @Column(name = "stud_accepted")
    private int studentsAccepted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "teacher_id")
    private TeacherDTO teacher;

    public Project() {
    }

    public Project(Long id, String name, int capacity, TeacherDTO teacher) {
        this.id = id;
        this.name = name;
        this.capacity = capacity;
        this.teacher = teacher;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public TeacherDTO getTeacher() {
        return teacher;
    }

    public void setTeacher(TeacherDTO teacher) {
        this.teacher = teacher;
    }

    public boolean isFull() {
        return (this.capacity == this.studentsAccepted);
    }

    public int getStudentsAccepted() {
        return studentsAccepted;
    }

    public void setStudentsAccepted(int studentsAccepted) {
        this.studentsAccepted = studentsAccepted;
    }

    public List<Student> getStudentList() {
        return studentList;
    }

    public void setStudentList(List<Student> studentList) {
        this.studentList = studentList;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "stud_pref", joinColumns = @JoinColumn(name = "proj_id"), inverseJoinColumns = @JoinColumn(name = "stud_id"))
    private List<Student> studentList = new ArrayList<Student>();

}