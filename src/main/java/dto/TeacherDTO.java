package dto;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alice on 12/6/2014.
 */
@javax.persistence.Entity
@Table( name = "teacher")
@NamedQueries({
        @NamedQuery(name = TeacherDTO.FIND_ALL, query = "from TeacherDTO")
}
)
public class TeacherDTO implements Serializable{
    public static final String FIND_ALL = "TeacherDTO.findAll";
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Id
    @GeneratedValue(  strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String username;

    @Column
    private String password;

    @Column
    private String name;

    @Column
    private Integer capacity;

    public List<Project> getStudentPreferencesList() {
        return studentPreferencesList;
    }

    public void setStudentPreferencesList(List<Project> studentPreferencesList) {
        this.studentPreferencesList = studentPreferencesList;
    }

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable( name="prof_pref", joinColumns = @JoinColumn( name="proj_id"), inverseJoinColumns = @JoinColumn (name = "stud_id"))
    private List<Project> studentPreferencesList = new ArrayList<Project>();

    public List<Project> getProjectList() {
        return projectList;
    }

    public void setProjectList(List<Project> projectList) {
        this.projectList = projectList;
    }

    @OneToMany( fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn( name = "teacher_id")
    List<Project> projectList;
}
