/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

/**
 *
 * @author lavinia
 */
public class Constants {

    public class Navigation {

        public static final String SUCCESS_STUD = "success_s";
        public static final String SUCCESS_TEACHER = "success_t";
        public static final String FAILURE = "failure";
        public static final String LOGOUT = "logout";
        public static final String APPLIED = "applied";
    }

    public interface EntityType {

        String STUDENT = "Student";
        String TEACHER = "Teacher";

    }
	
	public interface ReportParams {
        
        String STUDENTS_NO="studentsNo";
        String TEACHERS_NO="teachersNo";
        String PROJECTS_NO="projectsNo";
        String NOT_ACCEPTED_STUD="notAccStud";
    }
}
