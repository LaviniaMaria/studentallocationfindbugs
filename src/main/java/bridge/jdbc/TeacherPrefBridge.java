/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bridge.inteface.ITeacherPrefBridge;
import dto.Project;
import dto.Student;
import dto.TeacherDTO;

/**
 * 
 * @author lavinia
 */
public class TeacherPrefBridge implements ITeacherPrefBridge {

	@Override
	public void assignStudToProject(Student student, Project project) {

		insertProfPref(student, project);
		increaseStudNo(project);
	}

	@Override
	public TeacherDTO getTeacherById(Long id) {
		String query = "SELECT t.id, t.capacity, t.name, t.password, t.username FROM teacher t where t.id="
				+ id + ";";
		TeacherDTO teacher = null;
		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				teacher = new TeacherDTO();
				teacher.setId(rs.getLong(1));
				teacher.setName(rs.getString(3));
				teacher.setCapacity(rs.getInt(2));
				teacher.setPassword(rs.getString(4));
				teacher.setUsername(rs.getString(5));
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return teacher;
	}

	private void insertProfPref(Student student, Project project) {

		String query = "INSERT INTO prof_pref VALUES(?,?)";
		PreparedStatement pstmt = null;
		try {
			Connection conn = TableAccessBridge.getConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setLong(1, student.getId());
			pstmt.setLong(2, project.getTeacher().getId());
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

	private void increaseStudNo(Project project) {

		String query = "UPDATE PROJECT SET stud_accepted=? where id=?";
		int stud_accepted = project.getStudentsAccepted() + 1;
		PreparedStatement pstmt = null;
		try {
			Connection conn = TableAccessBridge.getConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setInt(1, stud_accepted);
			pstmt.setLong(2, project.getId());
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
