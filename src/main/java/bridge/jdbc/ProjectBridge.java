/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge.jdbc;

import bridge.inteface.IProjectBridge;
import dto.Project;
import dto.ProjectAssignment;
import dto.TeacherDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author lavinia
 */
public class ProjectBridge implements IProjectBridge {

	@Override
	public List<Project> getProjects(String studId) {
		System.out.println("jdbc");
		/*
		 * String query =
		 * "SELECT p.id,p.name,p.capacity,t.name,p.stud_accepted FROM project p, teacher t where p.teacher_id=t.id "
		 * +
		 * "AND p.id NOT IN (SELECT proj_id from stud_pref where stud_id='"+studId
		 * +"')";
		 */
		String query = "SELECT * from project";
		ArrayList<Project> projects = new ArrayList<Project>();
		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Project project = new Project();
				project.setId(rs.getLong("id"));
				project.setName(rs.getString("name"));
				project.setCapacity(Integer.parseInt(rs.getString("capacity")));
				TeacherDTO teacher = new TeacherDTO();
				teacher.setId(rs.getLong("id"));
				project.setTeacher(teacher);
				project.setStudentsAccepted(Integer.parseInt(rs
						.getString("stud_accepted")));
				projects.add(project);
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projects;

	}

	@Override
	public List<Project> getProjectsForTeacher(String teacherId) {

		String query = "SELECT p.id,p.name,p.capacity,t.name,p.stud_accepted FROM project p, teacher t where p.teacher_id=t.id "
				+ "AND p.teacher_id='" + teacherId + "'";

		ArrayList<Project> projects = new ArrayList<Project>();
		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Project project = new Project();
				project.setId(rs.getLong(1));
				project.setName(rs.getString(2));
				project.setCapacity(Integer.parseInt(rs.getString(3)));
				// TODO assign to teacher
				// project.setTeacher(rs.getLong(4));
				project.setStudentsAccepted(Integer.parseInt(rs.getString(5)));
				projects.add(project);
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projects;

	}

	@Override
	public void addProject(Project project) {

		String query = "INSERT INTO project VALUES(?,?,?,?)";
		PreparedStatement pstmt = null;
		try {
			Connection conn = TableAccessBridge.getConnection();
			pstmt = conn.prepareStatement(query);
			pstmt.setString(1, project.getName());
			pstmt.setString(2, String.valueOf(project.getCapacity()));
			pstmt.setLong(3, project.getTeacher().getId());
			pstmt.setString(4, String.valueOf(project.getStudentsAccepted()));
			pstmt.executeUpdate();

			pstmt.close();
			conn.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String getProjectsNo() {

		String query = "SELECT COUNT(*) FROM PROJECT";

		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return rs.getString(1);
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";

	}

	@Override
	public List<ProjectAssignment> getProjectsAssign() {

		String query = "select s.name AS \"SNAME\", p.name AS \"PNAME\", t.name AS \"TNAME\" from student s, project p, teacher t , prof_pref pp\n"
				+ "where pp.stud_id=s.id and pp.proj_id=p.id and p.teacher_id=t.id group by t.name,p.name,s.name";

		List<ProjectAssignment> projects = new ArrayList<ProjectAssignment>();
		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ProjectAssignment project = new ProjectAssignment();
				project.setProjectName(rs.getString("PNAME"));
				project.setStudentName(rs.getString("SNAME"));
				project.setTeacherName(rs.getString("TNAME"));
				projects.add(project);
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projects;

	}

}
