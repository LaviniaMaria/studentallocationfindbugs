/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author lavinia
 */
public class TableAccessBridge {

    public static Connection getConnection() throws Exception {

        Class.forName("oracle.jdbc.driver.OracleDriver");
        String host = "jdbc:oracle:thin:@localhost:1521:orcle";
        String uName = "system";
        String uPass = "lavinia92";
        Connection conn = DriverManager.getConnection(host, uName, uPass);
        System.out.println("Connected....");
        return conn;
    }

}
