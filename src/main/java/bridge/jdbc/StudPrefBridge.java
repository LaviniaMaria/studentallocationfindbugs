/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge.jdbc;

import bridge.inteface.IStudPrefBridge;
import dto.Student;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lavinia
 */
public class StudPrefBridge implements IStudPrefBridge{

    public void assignStudToProject(String studentId,String projectId) {

        String query = "INSERT INTO stud_pref VALUES(?,?)";
        PreparedStatement pstmt = null;
        try {
            Connection conn = TableAccessBridge.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setLong(1, Long.valueOf(studentId));
            pstmt.setLong(2, Long.valueOf(projectId));
            pstmt.executeUpdate();


            pstmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Student> getStudentsAt(String projectId) {
         
        List<Student> students=new ArrayList<Student>();
        String query = "Select s.id,s.name from stud_pref p,student s  where p.stud_id=s.id AND p.proj_id=?";
        PreparedStatement pstmt = null;
        try {
            Connection conn = TableAccessBridge.getConnection();
            pstmt = conn.prepareStatement(query);
            pstmt.setLong(1,Long.valueOf(projectId));
            ResultSet rs = pstmt.executeQuery();
            while(rs.next()) {
                Student student=new Student();
                student.setId(rs.getLong(1));
                student.setName(rs.getString(2));
                students.add(student);
            }

            pstmt.close();
            conn.close();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
        return students;
    }

}
