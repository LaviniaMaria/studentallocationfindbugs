/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bridge.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class UserBridge {

	public static String isUser(String username, String password, String table) {

		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;

		String query = "SELECT id FROM " + table.toLowerCase()
				+ " where username='" + username + "' and password='"
				+ password + "'";

		try {
			conn = TableAccessBridge.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);

			if (rs.next()) {
				return rs.getString(1);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != rs) {
				try {
					rs.close();
				} catch (final SQLException e) {
				}
			}
			if (null != stmt) {
				try {
					stmt.close();
				} catch (final SQLException e) {
				}
			}
		}

		return null;
	}

	public static boolean executeQuery(String query) {

		try {

			Connection conn = TableAccessBridge.getConnection();

			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				return true;
			}
			rs.close();
			stmt.close();
			conn.close();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return false;

	}

}
