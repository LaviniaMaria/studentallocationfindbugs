package bridge.jpa;

import bridge.inteface.ITeacherPrefBridge;
import dto.Project;
import dto.Student;
import dto.TeacherDTO;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Alice on 12/9/2014.
 */
public class TeacherPrefBridgeJPA implements ITeacherPrefBridge {

    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();

    @Override
    public void assignStudToProject(Student student, Project project) {
        List<Project> projectList = student.getProjectList();
        projectList.add(project);
        student.setProjectList(projectList);
        em.getTransaction().begin();
        em.persist(student);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public TeacherDTO getTeacherById(Long id){
        return em.find(TeacherDTO.class, id);
    }
}
