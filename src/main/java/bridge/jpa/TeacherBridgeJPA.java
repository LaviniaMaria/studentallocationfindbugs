package bridge.jpa;

import bridge.inteface.ITeacherBridge;
import dto.TeacherDTO;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by Alice on 1/5/2015.
 */
public class TeacherBridgeJPA implements ITeacherBridge {
    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();
    @Override
    public String getTeachersNo() {
        Integer teacherNo = em.createNamedQuery(TeacherDTO.FIND_ALL).getResultList().size();
        return teacherNo.toString();
    }
}
