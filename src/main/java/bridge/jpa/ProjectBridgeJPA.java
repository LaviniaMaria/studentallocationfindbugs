/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bridge.jpa;

import bridge.inteface.IProjectBridge;
import dto.Project;
import dto.ProjectAssignment;
import dto.TeacherDTO;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author alaic
 */
public class ProjectBridgeJPA implements IProjectBridge {

    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();

    @Override
    public List<Project> getProjects(String studId) {
        System.out.println("jpa");
        List<Project> projects = em.createNamedQuery(Project.FIND_ALL).getResultList();
        return projects;
    }

    @Override
    public List<Project> getProjectsForTeacher(String teacherId){
        List<Project> projects = em.createNamedQuery(Project.FIND_PROECTS_FOR_TEACHER).setParameter("teacher_id", Long.valueOf(teacherId)).getResultList();
        return projects;
    }

    @Override
    public void addProject(Project project) {
        TeacherDTO teacher = em.find(TeacherDTO.class, project.getTeacher().getId());

        em.getTransaction().begin();
        teacher.getProjectList().add(project);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public List<ProjectAssignment> getProjectsAssign() {
        //TODO hope it works
        Query q = em.createNativeQuery("select s.name AS studentName, p.name AS projectName, t.name AS teacherName from student s, project p, teacher t , prof_pref pp" +
                 " where pp.stud_id=s.id and pp.proj_id=p.id and p.teacher_id=t.id group by t.name,p.name,s.name", "ProjectAssignment");
        List<Object[]> lst = q.getResultList();
        List<ProjectAssignment> projectAssignments = new ArrayList<ProjectAssignment>();
        for (Iterator<Object[]> i = lst.iterator(); i.hasNext();)
        { Object[] obj = i.next(); System.out.println("codePerson:" +obj[0]); System.out.println("identif:" +obj[1]);
            ProjectAssignment p  = new ProjectAssignment();
            p.setStudentName((String)obj[0]);
            p.setProjectName((String)obj[1]);
            p.setTeacherName((String)obj[2]);
            projectAssignments.add(p);
        }
        return projectAssignments;
    }

    @Override
    public String getProjectsNo() {
        Integer maxNo = em.createNamedQuery(Project.FIND_ALL).getResultList().size();
        return maxNo.toString();
    }


}
