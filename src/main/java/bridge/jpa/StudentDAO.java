package bridge.jpa;

import dto.Student;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by Alice on 12/14/2014.
 */
public class StudentDAO {
    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();

    public List<Student> getAllStudent(){
       return em.createQuery(Student.FIND_ALL).getResultList();
    }
}
