package bridge.jpa;

import bridge.inteface.IStudPrefBridge;
import dto.Project;
import dto.Student;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alice on 12/7/2014.
 */

@Transactional
public class StudPrefBridgeJPA implements IStudPrefBridge{

    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();

    @Override
    public void assignStudToProject(String studentId, String projectId) {
        Project project = em.find(Project.class, Long.valueOf(projectId));
        Student student = em.find(Student.class, Long.valueOf(studentId));
        em.getTransaction().begin();
        boolean added  = project.getStudentList().add(student);
        em.getTransaction().commit();
        em.close();
        System.out.println("Added : " + added);
    }

    @Override
    public List<Student> getStudentsAt(String projectId) {
        Project p  = em.find(Project.class, Long.valueOf(projectId));
        return p.getStudentList();
     }

    public List<Project> getProjectsForTeacherThatAreFull(Long teacherId){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        final CriteriaQuery criteriaQuery = criteriaBuilder.createQuery(Project.class);

        final Root projectRoot = criteriaQuery.from(Project.class);

        List<Predicate> criteriaList = new ArrayList<Predicate>();

        Predicate teacherPredicate = criteriaBuilder.equal(projectRoot.get("teacher").get("id"), teacherId);
        Predicate projectFull = criteriaBuilder.equal(projectRoot.get("capacity"), projectRoot.get("studentsAccepted"));

        criteriaList.add(teacherPredicate);
        criteriaList.add(projectFull);

        criteriaQuery.where(criteriaBuilder.and(criteriaList.toArray(new Predicate[0])));

        criteriaQuery.orderBy(criteriaBuilder.asc(projectRoot.get("name")));
        final TypedQuery query = em.createQuery(criteriaQuery);
        return query.getResultList();
    }


}
