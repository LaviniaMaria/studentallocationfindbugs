package bridge.jpa;

import bridge.inteface.IStudentBridge;
import dto.Student;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;

/**
 * Created by Alice on 1/5/2015.
 */
public class StudentBridgeJPA implements IStudentBridge {

    EntityManager em = Persistence.createEntityManagerFactory("homework").createEntityManager();

    @Override
    public String getStudentsNo() {
        Integer studNo = em.createNamedQuery(Student.FIND_ALL).getResultList().size();
        return studNo.toString();
    }
}
