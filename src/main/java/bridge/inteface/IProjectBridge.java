/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bridge.inteface;

import dto.Project;
import dto.ProjectAssignment;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author alaic
 */
public interface IProjectBridge {
    public List<Project> getProjects(String studId);

    public List<Project> getProjectsForTeacher(String teacherId);

    public void addProject(Project project);

    public List<ProjectAssignment> getProjectsAssign();

    public String getProjectsNo();
    
}
