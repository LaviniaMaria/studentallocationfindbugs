package bridge.inteface;

import dto.Student;

import java.util.List;

/**
 * Created by Alice on 12/6/2014.
 */
public interface IStudPrefBridge {
    public void assignStudToProject(String studentId,String projectId);
    public List<Student> getStudentsAt(String projectId);
}
