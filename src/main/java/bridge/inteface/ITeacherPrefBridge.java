package bridge.inteface;

import dto.Project;
import dto.Student;
import dto.TeacherDTO;

/**
 * Created by Alice on 12/9/2014.
 */
public interface ITeacherPrefBridge {
    public void assignStudToProject(Student student, Project project);
    public TeacherDTO getTeacherById(Long id);
}
