package dao;

import bridge.inteface.*;
import bridge.jdbc.*;

/**
 * Created by Alice on 12/6/2014.
 */
public class JDBCFactory extends Factory{
    @Override
    public IProjectBridge getProjectDAO() {
        return new ProjectBridge();
    }

    @Override
    public IStudPrefBridge getStudPrefDAO() {
        return new StudPrefBridge();
    }

    @Override
    public ITeacherPrefBridge getTeacherPrefDAO() {
        return new TeacherPrefBridge();
    }

    @Override
    public ITeacherBridge getTeacherDAO() {
        return new TeacherBridge();
    }

    @Override
    public IStudentBridge getStudentDAO() {
        return new StudentBridge();
    }


}
