package dao;

import bridge.inteface.*;

/**
 * Created by Alice on 12/6/2014.
 */
public abstract class Factory {
    // Supported data types
    public static final int JDBC = 1;
    public static final int JPA = 2;

    // Abstract access to DAO
    public abstract IProjectBridge getProjectDAO();
    public abstract IStudPrefBridge getStudPrefDAO();
    public abstract ITeacherPrefBridge getTeacherPrefDAO();
    public abstract ITeacherBridge getTeacherDAO();
    public abstract IStudentBridge getStudentDAO();

    // Method for creating a specific factory
    public static Factory getDAOFactory(int whichFactory) {
        switch (whichFactory) {
            case JDBC :
                return new JDBCFactory();
            case JPA :
                return new JPAFactory();
            default: return new JDBCFactory();
        }
    }
}
