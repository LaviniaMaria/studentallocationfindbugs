package dao;

import bridge.inteface.*;
import bridge.jpa.*;

/**
 * Created by Alice on 12/6/2014.
 */
public class JPAFactory extends Factory {
    @Override
    public IProjectBridge getProjectDAO() {
        return new ProjectBridgeJPA();
    }

    @Override
    public IStudPrefBridge getStudPrefDAO() {
        return new StudPrefBridgeJPA();
    }

    @Override
    public ITeacherPrefBridge getTeacherPrefDAO() {
        return new TeacherPrefBridgeJPA();
    }

    @Override
    public ITeacherBridge getTeacherDAO() {
        return new TeacherBridgeJPA();
    }

    @Override
    public IStudentBridge getStudentDAO() {
        return new StudentBridgeJPA();
    }
}
